<?php

namespace AppBundle\Validator;

interface Validator
{
    public function validate($data);
}
