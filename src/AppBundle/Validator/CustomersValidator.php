<?php

namespace AppBundle\Validator;

class CustomersValidator implements Validator
{
    private $validators;

    public function validate($customers)
    {
        foreach ($customers as $customer) {
            foreach ($this->validators as $validator) {
                $validator->validate($customer);
            }
        }
    }

    public function addValidator(Validator $validator)
    {
        $this->validators[] = $validator;
        return $this;
    }
}
