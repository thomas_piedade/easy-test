<?php

namespace AppBundle\Validator\Factory;

use AppBundle\Validator\NameValidator;
use AppBundle\Validator\AgeValidator;
use AppBundle\Validator\CustomersValidator;

class CustomersValidatorFactory
{
    public function create()
    {
        return (new CustomersValidator)
            ->addValidator(new NameValidator())
            ->addValidator(new AgeValidator());
    }
}
