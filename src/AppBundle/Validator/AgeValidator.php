<?php

namespace AppBundle\Validator;

use AppBundle\Exception\InvalidCustomerDataException;

class AgeValidator implements Validator
{
    private $customer;

    public function validate($customer)
    {
        $this->customer = $customer;
        if (!$this->isValid()) {
            throw new InvalidCustomerDataException('This customer has invalid age');
        }
    }

    private function isValid()
    {
        return isset($this->customer->age)
            && !empty($this->customer->age)
            && is_numeric($this->customer->age)
            && $this->customer->age > 0;
    }

}
