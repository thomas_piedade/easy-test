<?php

namespace AppBundle\Validator;

use AppBundle\Exception\InvalidCustomerDataException;

class NameValidator implements Validator
{
    private $customer;

    public function validate($customer)
    {
        $this->customer = $customer;
        if (!$this->isValid()) {
            throw new InvalidCustomerDataException('This customer has invalid name');
        }
    }

    private function isValid()
    {
        return isset($this->customer->name)
            && !empty($this->customer->name)
            && $this->hasValidSize();

    }

    private function hasValidSize()
    {
        return strlen($this->customer->name) > 1
            && strlen($this->customer->name) <= 100;
    }
}
