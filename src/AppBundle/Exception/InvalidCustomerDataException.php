<?php

namespace AppBundle\Exception;

class InvalidCustomerDataException extends \Exception
{
  public function __construct($message) {
      parent::__construct($message);
  }
}
