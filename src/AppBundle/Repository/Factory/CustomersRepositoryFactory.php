<?php

namespace AppBundle\Repository\Factory;

use AppBundle\Repository\CustomersRepository;

class CustomersRepositoryFactory
{
    private $customersDatabaseService;
    private $customersCacheService;

    public function create($customersDatabaseService, $customersCacheService)
    {
        return new CustomersRepository(
            $customersDatabaseService,
            $customersCacheService
        );
    }
}
