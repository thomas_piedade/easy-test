<?php

namespace AppBundle\Repository;

class CustomersRepository
{
    private $customersDatabaseService;
    private $customersCacheService;

    public function __construct($customersDatabaseService, $customersCacheService)
    {
        $this->customersDatabaseService = $customersDatabaseService;
        $this->customersCacheService    = $customersCacheService;
    }

    public function findAll()
    {
        try {
            $customers = $this->customersCacheService->findAll();
            if (is_null($customers)) {
                $customers = $this->customersDatabaseService->findAll();
                $this->customersCacheService->insert($customers);
            }
        } catch (\Predis\Connection\ConnectionException $ce) {
            $customers = $this->customersDatabaseService->findAll();
        }

        return $customers;
    }

    public function deleteAll()
    {
        try {
            $this->customersCacheService->deleteAll();
            $this->customersDatabaseService->deleteAll();
        } catch (\Predis\Connection\ConnectionException $ce) {
            $this->customersDatabaseService->deleteAll();
        }
    }

    public function insert($customers)
    {
        try {
            $this->customersCacheService->deleteAll();
            $this->customersCacheService->insert($customers);
            $this->customersDatabaseService->insert($customers);
        } catch (\Predis\Connection\ConnectionException $ce) {
            $this->customersDatabaseService->insert($customers);
        }
    }
}
