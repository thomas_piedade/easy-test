<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use AppBundle\Exception\InvalidCustomerDataException;

class CustomersController extends Controller
{
    /**
     * @Route("/customers/")
     * @Method("GET")
     */
    public function getAction()
    {
        $response['customers'] = $this->get('customers_repository')->findAll();
        return new JsonResponse($response);
    }

    /**
     * @Route("/customers/")
     * @Method("POST")
     */
    public function postAction(Request $request)
    {
        $customers = json_decode($request->getContent());

        if (empty($customers)) {
            return new JsonResponse(['status' => 'No donuts for you'], 412);
        }
        try {
            $this->get('customers_validator')->validate($customers);
            $this->get('customers_repository')->insert($customers);
        } catch (InvalidCustomerDataException $e) {
            return new JsonResponse(['status' => $e->getMessage()], 412);
        } catch (Exception $e) {
            return new JsonResponse(['status' => $e->getMessage()], 500);
        }

        return new JsonResponse(
            ['status' => 'Customers successfully created'],
            201
        );
    }

    /**
     * @Route("/customers/")
     * @Method("DELETE")
     */
    public function deleteAction()
    {
        $this->get('customers_repository')->deleteAll();

        return new JsonResponse(
            ['status' => 'Customers successfully deleted'],
            204
        );
    }
}
