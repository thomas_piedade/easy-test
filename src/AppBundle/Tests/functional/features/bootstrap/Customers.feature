Feature: Customers

    Scenario: Create customers with valid data

        Given I want to insert customers
        When I send a "POST" request
        Then the status code response should be "201"

    Scenario: Create customers with empty data

        Given I want to insert customers whit empty data
        When I send a "POST" request
        Then the status code response should be "412"

    Scenario: Create customers with invalid data

        Given I not define any customer
        When I send a "POST" request
        Then the status code response should be "412"

    Scenario: Delete customers

        Given I send a "DELETE" request
        Then the status code response should be "204"

    Scenario: Get customers

        Given I send a "GET" request
        Then the status code response should be "200"
