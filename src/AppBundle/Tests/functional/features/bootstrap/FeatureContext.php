<?php

use Behat\Behat\Tester\Exception\PendingException;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use GuzzleHttp\Client;

require_once '../../../../vendor/phpunit/phpunit/src/Framework/Assert/Functions.php';

/**
 * Behat context class.
 */
class FeatureContext implements SnippetAcceptingContext
{

    private $response;
    private $customers;
    const CUSTOMERS_URL = 'http://127.0.0.1:8000/customers/';
    /**
     * Initializes context.
     *
     * Every scenario gets it's own context object.
     * You can also pass arbitrary arguments to the context constructor through behat.yml.
     */
    public function __construct()
    {
    }

    /**
     * @Given I want to insert customers
     */
    public function iWantToInsertCustomers()
    {
        $this->customers = '[{"name":"leandro", "age":26}, {"name":"marcio", "age":30}]';
    }

    /**
     * @When I send a :arg1 request
     */
    public function iSendARequest($method)
    {
        $client = new Client();
        $request = $client->createRequest($method, self::CUSTOMERS_URL, ['body' => $this->customers]);

        try {
            $this->response = $client->send($request);
        }catch(\Exception $e) {
            $this->response = $e->getResponse();
        }
    }

    /**
     * @Then the status code response should be :arg1
     */
    public function theStatusCodeResponseShouldBe($statusCode)
    {
        assertEquals($statusCode, $this->response->getStatusCode());
    }

    /**
     * @Given I not define any customer
     */
    public function iNotDefineAnyCustomer()
    {
        $this->customers = '[{"name":"", "age":26}, {"name":"marcio", "age":30}]';
    }

    /**
     * @Given I want to insert customers whit empty data
     */
    public function iWantToInsertCustomersWhitEmptyData()
    {
        $this->customers = '';
    }
}
