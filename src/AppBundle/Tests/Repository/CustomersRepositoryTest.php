<?php

namespace AppBundle\Tests\Repository;

use AppBundle\Tests\MyTestCase;

class CustomersRepositoryTest extends MyTestCase
{
    private $container;
    private $customersRepository;

    public function setUp()
    {
        $this->client = self::createClient();
        $this->customersRepository = $this->client->getContainer()
            ->get('customers_repository');
    }

    public function testClassCustomersRepositoryExists()
    {
        $this->assertInstanceOf(
          'AppBundle\Repository\CustomersRepository',
          $this->customersRepository
        );
    }

    public function testCreateCustomers()
    {
        $this->getMockDatabaseService('insert', $return = null);
        $this->getMockCacheService('insert', $return = null);

        $customers = [
            ['name' => 'Leandro', 'age' => 26],
            ['name' => 'Marcelo', 'age' => 30],
            ['name' => 'Alex', 'age' => 18],
        ];

        $this->customersRepository->insert($customers);
    }

    public function testGetCustomersByCache()
    {
        $expectedReturn = '{"customers":[{"name":"leandro","age":26},{"name":"marcio","age":30}]}';

        $mockDatabaseService = $this->getMockDatabaseService('findAll');
        $mockCacheService = $this->getMockCacheService('findAll', $expectedReturn);

        $this->setKernelModifier(function () use ($mockDatabaseService, $mockCacheService) {
          static::$kernel->getContainer()->set('customers_database_service', $mockDatabaseService);
          static::$kernel->getContainer()->set('customers_cache_service', $mockCacheService);
        });

        $client = static::createClient();
        $customers = $client->getContainer()
            ->get('customers_repository')
            ->findAll();

        $this->assertEquals($customers, $expectedReturn);
    }

    public function testGetCustomersByDatabase()
    {
        $expectedReturn = '{"customers":[{"name":"leandro","age":26},{"name":"marcio","age":30}]}';

        $mockDatabaseService = $this->getMockDatabaseService('findAll', $expectedReturn);
        $mockCacheService = $this->getMockCacheService('findAll');

        $this->setKernelModifier(function () use ($mockDatabaseService, $mockCacheService) {
          static::$kernel->getContainer()->set('customers_database_service', $mockDatabaseService);
          static::$kernel->getContainer()->set('customers_cache_service', $mockCacheService);
        });

        $client = static::createClient();
        $customers = $client->getContainer()
            ->get('customers_repository')
            ->findAll();

        $this->assertEquals($customers, $expectedReturn);
    }

    private function getMockDatabaseService($method, $return = null)
    {
        $customersDatabaseServiceMock = $this->getMockBuilder('AppBundle\Service\CustomersDatabaseService')
            ->disableOriginalConstructor()
            ->getMock();

        $customersDatabaseServiceMock->method($method)
            ->willReturn($return);

        return $customersDatabaseServiceMock;
    }

    private function getMockCacheService($method, $return = null)
    {
        $customersCacheServiceMock = $this->getMockBuilder('AppBundle\Service\CustomersCacheService')
            ->disableOriginalConstructor()
            ->getMock();

        $customersCacheServiceMock->method($method)
            ->willReturn($return);

        return $customersCacheServiceMock;
    }
}
