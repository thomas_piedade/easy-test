<?php

namespace AppBundle\Tests\Repository;

use AppBundle\Tests\MyTestCase;
use AppBundle\Validator\AgeValidator;

class AgeValidatorTest extends MyTestCase
{
    private $validator;

    public function setUp()
    {
        $this->validator = new AgeValidator();
    }

    public function testClassAgeValidatorExists()
    {
        $this->assertInstanceOf(
          'AppBundle\Validator\AgeValidator',
          $this->validator
        );
    }

    public function testWhitValidAgeNotReturnException()
    {
        $customer = $this->createCustomer(19);
        $this->validator->validate($customer);
    }

    /**
     * @expectedException AppBundle\Exception\InvalidCustomerDataException
     */
    public function testWhitNullAgeReturnException()
    {
        $customer = $this->createCustomer(null);
        $this->validator->validate($customer);
    }

    /**
     * @expectedException AppBundle\Exception\InvalidCustomerDataException
     */
    public function testWhitEmptyAgeReturnException()
    {
        $customer = $this->createCustomer('');
        $this->validator->validate($customer);
    }

    /**
     * @expectedException AppBundle\Exception\InvalidCustomerDataException
     */
    public function testWhitZeroAgeReturnException()
    {
        $customer = $this->createCustomer(0);
        $this->validator->validate($customer);
    }

    /**
     * @expectedException AppBundle\Exception\InvalidCustomerDataException
     */
    public function testWhitInvalidAgeReturnException()
    {
        $customer = $this->createCustomer('test');
        $this->validator->validate($customer);
    }


    private function createCustomer($age = null)
    {
        $customer = new \StdClass;
        $customer->age = $age;
        return $customer;
    }
}
