<?php

namespace AppBundle\Tests\Repository;

use AppBundle\Tests\MyTestCase;
use AppBundle\Validator\NameValidator;

class NameValidatorTest extends MyTestCase
{
    private $validator;

    public function setUp()
    {
        $this->validator = new NameValidator();
    }

    public function testClassNameValidatorExists()
    {
        $this->assertInstanceOf(
          'AppBundle\Validator\NameValidator',
          $this->validator
        );
    }

    public function testWhitValidNameNotReturnException()
    {
        $customer = $this->createCustomer('thomas');
        $this->validator->validate($customer);
    }

    /**
     * @expectedException AppBundle\Exception\InvalidCustomerDataException
     */
    public function testWhitNullNameReturnException()
    {
        $customer = $this->createCustomer(null);
        $this->validator->validate($customer);
    }

    /**
     * @expectedException AppBundle\Exception\InvalidCustomerDataException
     */
    public function testWhitEmptyNameReturnException()
    {
        $customer = $this->createCustomer('');
        $this->validator->validate($customer);
    }

    /**
     * @expectedException AppBundle\Exception\InvalidCustomerDataException
     */
    public function testWhitInvalidNameReturnException()
    {
        $customer = $this->createCustomer('t');
        $this->validator->validate($customer);
    }

    private function createCustomer($name = null)
    {
        $customer = new \StdClass;
        $customer->name = $name;
        return $customer;
    }
}
