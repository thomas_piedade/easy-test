<?php

namespace AppBundle\Service;

class CustomersCacheService
{
    private $cacheService;
    const CUSTOMERS_CACHE_KEY = 'customers';

    public function __construct($cacheService)
    {
        $this->cacheService = $cacheService;
    }

    public function findAll()
    {
        $customers = $this->cacheService->get(self::CUSTOMERS_CACHE_KEY);

        if ($customers) {
            return $customers;
        }

        return null;
    }

    public function insert($customers)
    {
        $this->cacheService->set(
            self::CUSTOMERS_CACHE_KEY,
            serialize($customers)
        );
    }

    public function deleteAll()
    {
        $this->cacheService->del(self::CUSTOMERS_CACHE_KEY);
    }
}
