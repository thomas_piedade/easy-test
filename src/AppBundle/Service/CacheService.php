<?php

namespace AppBundle\Service;

use Predis;

/**
* Here you have to implement a CacheService with the operations above.
* It should contain a failover, which means that if you cannot retrieve
* data you have to hit the Database.
**/
class CacheService
{
    private $redis;

    public function __construct($host, $port, $prefix)
    {
        $this->redis = new Predis\Client(
            [
                'host' => $host,
                'port' => $port
            ]
        );
    }

    public function get($key)
    {
        return unserialize(
            $this->redis->get($key)
        );
    }

    public function set($key, $value)
    {
        $this->redis->set($key, $value);
    }

    public function del($key)
    {
        $this->redis->del($key);
    }
}
