<?php

namespace AppBundle\Service;

class CustomersDatabaseService
{
    private $customersDatabase;

    public function __construct($databaseService)
    {
        $this->databaseService = $databaseService;
        $database = $this->databaseService->getDatabase();
        $this->customersDatabase = $database->customers;
    }

    public function findAll()
    {
        $customers = $this->customersDatabase->find();
        $customers = iterator_to_array($customers);
        return $customers;
    }

    public function insert($customers)
    {
        foreach ($customers as $customer) {
            $this->customersDatabase->insert($customer);
        }
    }

    public function deleteAll()
    {
        $this->customersDatabase->drop();
    }
}
